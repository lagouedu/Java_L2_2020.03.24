## 实例

下面我们通过例子来理解 Paxos 的实际应用过程。

假设现在有五个节点的分布式系统，此时 A 节点打算提议 X 值，E 节点打算提议 Y 值，其他节点没有提议。
[![Paxos-1](https://static.oschina.net/uploads/img/201802/02103739_Oi93.png)](http://7xjtfr.com1.z0.glb.clouddn.com/Paxos-1.png)Paxos-1

假设现在 A 节点广播它的提议（也会发送给自己），由于网络延迟的原因，只有 A，B，C 节点收到了。注意即使 A，E 节点的提议同时到达某个节点，它也必然有个先后处理的顺序，这里的“同时”不是真正意义上的“同时”。
[![Paxos-2](https://static.oschina.net/uploads/img/201802/02103739_AQaB.png)](http://7xjtfr.com1.z0.glb.clouddn.com/Paxos-2.png)Paxos-2

A，B，C接收提议之后，由于这是第一个它们接收到的提议，acceptedProposal 和 acceptedValue 都为空。
[![Paxos-3](https://static.oschina.net/uploads/img/201802/02103739_7yWf.png)](http://7xjtfr.com1.z0.glb.clouddn.com/Paxos-3.png)Paxos-3

由于 A 节点已经收到超半数的节点响应，且返回的 acceptedValue 都为空，也就是说它可以用 X 作为提议的值来发生 Accept 请求，A，B，C接收到请求之后，将 acceptedValue 更新为 X。
[![Paxos-4](https://static.oschina.net/uploads/img/201802/02103739_dDVv.png)](http://7xjtfr.com1.z0.glb.clouddn.com/Paxos-4.png)Paxos-4

A，B，C 会发生 minProposal 给 A，A 检查发现没有大于 1 的 minProposal 出现，此时 X 已经被选中。等等，我们是不是忘了D，E节点？它们的 acceptedValue 并不是 X，系统还处于不一致状态。至此，Paxos 过程还没有结束，我们继续看。
[![Paxos-5](https://static.oschina.net/uploads/img/201802/02103739_HhfX.png)](http://7xjtfr.com1.z0.glb.clouddn.com/Paxos-5.png)Paxos-5

此时 E 节点选择 Proposal ID 为 2 发送 Prepare 请求，结果就和上面不一样了，因为 C 节点已经接受了 A 节点的提议，它不会三心二意，所以就告诉 E 节点它的选择，E 节点也很绅士，既然 C 选择了 A 的提议，那我也选它吧。于是，E 发起 Accept 请求，使用 X 作为提议值，至此，整个分布式系统达成了一致，大家都选择了 X。
[![Paxos-6](https://static.oschina.net/uploads/img/201802/02103739_Ja4T.png)](http://7xjtfr.com1.z0.glb.clouddn.com/Paxos-6.png)Paxos-6





-----------------

![image-20200422141705550](/Users/ericsun/Library/Application Support/typora-user-images/image-20200422141705550.png)